# Wireguard VPN using Docker

# Getting Started
```
1. clone repo ini
2. jalankan command:
   $ docker-compose up -d
3. open port 51280 di router forward ke server wireguard nya
4. lalu copy file config/peer1/peer1.conf -> local pc
5. di local pc install wireguard
   $ sudo apt install wireguard
6. copy file peer1.conf -> /etc/wireguard/wg0.conf
7. jalankan sudo wg-quick up wg0 // untuk koneksi vpn
8. jalankan sudo wg-quick down wg0 // untuk mematikan koneksi vpn
```
